package lab2.logariphmic;

import lab2.Calculator;

public class LnCalculator extends Calculator {

    public LnCalculator(double accuracy) {
        super(accuracy);
    }

    public double calculate(double x) {
        if (Double.isNaN(x) || x < 0.0) {
            return Double.NaN;
        } else if (x == Double.POSITIVE_INFINITY) {
            return Double.POSITIVE_INFINITY;
        } else if (x == 0.0) {
            return Double.NEGATIVE_INFINITY;
        }

        double prev = 10;
        double current = 0;
        int n = 1;

        while(Math.abs(prev - current) >= getAccuracy()){
            prev = current;
            current += Math.pow((x - 1) / (x + 1), n) / n;
            n += 2;
        }

        double value = 2 * current;
        return value;
    }
}
