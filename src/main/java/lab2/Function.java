package lab2;

import lab2.logariphmic.*;
import lab2.trigonometric.*;

import static java.lang.Math.PI;

public class Function extends Calculator {

    private SinCalculator sin;
    private CosCalculator cos;
    private TanCalculator tan;
    private CotanCalculator cotan;
    private SecCalculator sec;
    private CosecCalculator cosec;
    private LnCalculator ln;
    private LogBaseCalculator log2;
    private LogBaseCalculator log5;
    private LogBaseCalculator log10;

    public Function(double accuracy, SinCalculator sin, CosCalculator cos, TanCalculator tan, CotanCalculator cotan,
                    SecCalculator sec, CosecCalculator cosec, LnCalculator ln, LogBaseCalculator log2,
                    LogBaseCalculator log5, LogBaseCalculator log10) {
        super(accuracy);
        this.sin = sin;
        this.cos = cos;
        this.tan = tan;
        this.cotan = cotan;
        this.sec = sec;
        this.cosec = cosec;
        this.ln = ln;
        this.log2 = log2;
        this.log5 = log5;
        this.log10 = log10;

    }

    public double calculate(double x){
        System.out.println(sin.calculate(x));

        if (x <= 0) {
            return ((((sec.calculate(x) - cotan.calculate(x))   -  cotan.calculate(x))   -  sin.calculate(x))   + cos.calculate(x)) /
            (((tan.calculate(x) + prod(cos.calculate(x),3))  * sin.calculate(x)) - cosec.calculate(x));
        } else {
            return                prod(((prod(ln.calculate(x),3) - (log5.calculate(x) - log10.calculate(x))) / ln.calculate(x)),2)
                    *(ln.calculate(x) - (prod(log2.calculate(x),2) + ln.calculate(x)));
        }
    }

    private static double prod(double x, int n) {
        double accum = 1;

        for (int i = 1; i <= n; i++) {
            accum *= x;
        }
        return accum;
    }
}
