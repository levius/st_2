package lab2;

public abstract class Calculator {

    public double getAccuracy() {
        return accuracy;
    }

    private double accuracy;

    public Calculator(double accuracy) {
        this.accuracy = accuracy;
    }

    public abstract double calculate(double x);

}
