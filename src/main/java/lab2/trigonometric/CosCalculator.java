package lab2.trigonometric;

import lab2.Calculator;

public class CosCalculator extends Calculator {

    private SinCalculator sin;

    public CosCalculator(double accuracy, SinCalculator sin) {
        super(accuracy);
        this.sin = sin;
    }

    public double calculate(double x) {
        int sign= 1;

        if ((x > Math.PI/2) & (x < 3 * Math.PI/2))
            sign = -1;

        if ((x < -Math.PI/2) & (x > -3 * Math.PI/2))
            sign = -1;

        return sign * Math.sqrt(1 - sin.calculate(x) * sin.calculate(x));
    }

}